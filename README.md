JBricks
=

JBricks is a Java game developed by Mohammad Anas Al-Mahdi , Mohammad Al-Najjar and Ammar Lakis ; students at the university of damascus

-----
##Developping

1. [Install Git](http://www.git-scm.com/) on your system.
2. Clone the repo  `git clone git@bitbucket.org:Mhmad_Alnajjar/break_bricks.git`
3. Import the project file to eclipse workspace.

-----
##Updates :

1.3 changelog :

   - Link to wiki in help menu.
   - Correct sound option in options menu


1.2 changelog :

   - Undo option fixed.


1.1 changelog :

   - Adding on/off sound option in options menu.
   - Fixing statistics table.

