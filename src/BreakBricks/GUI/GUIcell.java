package BreakBricks.GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import BreakBricks.Core.ColorsGroup;

@SuppressWarnings("serial")
/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 *
 */
public class GUIcell extends JPanel {
	/**
	 * Grid that holds this cell
	 */
	private GUIgrid grid;

	/**
	 * Cloumn number
	 */
	private int col;

	/**
	 * Row number
	 */
	private int row;

	/**
	 * Is Marked
	 */
	private boolean marked = false;

	/**
	 * Width of cell
	 */
	public static int width;

	/**
	 * Old width of cell
	 */
	public static int oldWidth = -1;

	/**
	 * Height of cell
	 */
	public static int height;

	/**
	 * Old height of cell
	 */
	public static int oldheight = -1;

	/**
	 * Color Index.
	 */
	private Integer color;

	/**
	 * Constructor
	 * 
	 * @param grid
	 *            grid that holds this cell
	 * @param row
	 *            row num
	 * @param col
	 *            col num
	 * @param colorIndex
	 *            color Index
	 */
	GUIcell(GUIgrid grid, int row, int col, Integer colorIndex) {
		super();
		moveTo(row, col);
		this.setGrid(grid);
		width = getWidth();
		height = getHeight();

		setColor(colorIndex);
		setForeground(Color.BLACK);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));

		Event e = new Event();
		this.addMouseListener(e);
	}

	/**
	 * To Listen on mouse Action on this cell
	 * 
	 * @author Mhmad
	 * 
	 */
	public class Event implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			if (getBackground() != null)
				grid.getMainGUI().click(row, col);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			if (getBackground() != null)
				grid.getMainGUI().over(row, col);
		}

		@Override
		public void mouseExited(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {

		}

		@Override
		public void mouseReleased(MouseEvent e) {

		}
	}

	/**
	 * to resize all cells dimensions..
	 * 
	 * @param newWidth
	 * @param newHeight
	 */
	static void resized(int newWidth, int newHeight) {
		oldWidth = width;
		oldheight = height;
		width = newWidth;
		height = newHeight;
	}

	/**
	 * move cell to anthor index
	 * 
	 * @param row
	 *            row num
	 * @param col
	 *            column num
	 * @return true if the change take in
	 */
	public boolean moveTo(int row, int col) {
		this.col = col;
		this.row = row;
		return true;
	}

	/*
	 * main.. public boolean setColor(Integer color) { if (this.color != color)
	 * { this.color = color; if (color == null) setBackground(null); else
	 * setBackground(ColorsGroup.getColor(color)); return true; } return false;
	 * 
	 * }
	 */

	public boolean setColor(Integer color) {
		if (this.color != color) {
			this.color = color;
			return true;
		}
		return false;

	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, width, height);
		if (marked == true) {
			g.setColor(Color.lightGray);
			g.fillRect(0, 0, width, height);
		}
		if (color != null) {
			if (width != oldWidth || height != oldheight) {

				ColorsGroup.setColors(null, getWidth(), getHeight());

				oldWidth = width;
				oldheight = height;
			}
			g.drawImage(ColorsGroup.getImage(color), 0, 0, this);
		}
	}

	/**
	 * set Marked
	 * 
	 * @param is
	 *            Marked
	 */
	public void setMarked(boolean is) {
		marked = is;
	}

	/**
	 * Get Color Index
	 * 
	 * @return returns the cell color
	 */
	public int getColor() {
		return this.color;
	}

	/**
	 * @return the grid
	 */
	public GUIgrid getGrid() {
		return grid;
	}

	/**
	 * @param grid
	 *            the grid to set
	 */
	public void setGrid(GUIgrid grid) {
		this.grid = grid;
	}
}
