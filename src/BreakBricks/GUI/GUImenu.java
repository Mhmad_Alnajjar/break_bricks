package BreakBricks.GUI;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URI;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

import BreakBricks.Core.ColorsGroup;
import BreakBricks.Core.Game;
import BreakBricks.Settings.SaveLoadGame;
import BreakBricks.Settings.Settings;

/**
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 * 
 */
@SuppressWarnings("serial")
public class GUImenu extends JMenuBar {
	/**
	 * Main Panel that holds this menubar
	 */
	private MainGUI mainGUI;

	/**
	 * Main Menu
	 */
	private JMenu mainMenu;

	/**
	 * Option Menu
	 */
	private JMenu optionsMenu;

	/**
	 * Help Menu
	 */
	private JMenu helpMenu;

	// Submenus
	private JMenu gameMenu;
	private JMenu difficultyMenu;

	// All Items in main menu
	private JMenuItem newGame;
	private JMenuItem saveGame;
	private JMenuItem loadGame;
	private JMenuItem undo;
	private JMenuItem statistics;
	private JMenuItem exitGame;

	// All Items in options menu
	private JMenuItem easyLevel;
	private JMenuItem normalLevel;
	private JMenuItem hardLevel;
	private JMenuItem customLevel;
	private JCheckBoxMenuItem soundToggle;

	// All Items in help menu
	private JMenuItem wiki;
	private JMenuItem bugSubmit;
	private JMenuItem forkUs;
	private JMenuItem about;

	/**
	 * Constructor
	 * 
	 * @param mainGUI
	 *            Main panel hold this menubar
	 */
	public GUImenu(MainGUI mainGUI) {
		this.mainGUI = mainGUI;

		// Intilize File Menu and Its Component..
		mainMenu = new JMenu("Main");
		add(mainMenu);

		gameMenu = new JMenu("Game");
		mainMenu.add(gameMenu);

		newGame = new JMenuItem("New");
		newGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		gameMenu.add(newGame);

		saveGame = new JMenuItem("Save");
		saveGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		gameMenu.add(saveGame);

		loadGame = new JMenuItem("Load");
		loadGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		gameMenu.add(loadGame);

		undo = new JMenuItem("Undo");
		undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit
				.getDefaultToolkit().getMenuShortcutKeyMask()));
		undo.setEnabled(false);
		mainMenu.add(undo);

		statistics = new JMenuItem("Statistics");
		statistics.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		statistics.setEnabled(true);
		mainMenu.add(statistics);

		mainMenu.add(new JSeparator());

		exitGame = new JMenuItem("Exit");
		exitGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
		mainMenu.add(exitGame);

		// Intilize Levels Menu and Its Component..
		optionsMenu = new JMenu("Options");
		add(optionsMenu);

		difficultyMenu = new JMenu("Difficulty");
		optionsMenu.add(difficultyMenu);

		easyLevel = new JMenuItem("Easy");
		difficultyMenu.add(easyLevel);

		normalLevel = new JMenuItem("Normal");
		difficultyMenu.add(normalLevel);

		hardLevel = new JMenuItem("Hard");
		difficultyMenu.add(hardLevel);

		customLevel = new JMenuItem("Custom Settings");
		optionsMenu.add(customLevel);

		optionsMenu.add(new JSeparator());

		soundToggle = new JCheckBoxMenuItem("Sound off");
		optionsMenu.add(soundToggle);

		// Intilize Help Menu and Its Component..
		helpMenu = new JMenu("Help");
		add(helpMenu);

		wiki = new JMenuItem("Wiki");
		helpMenu.add(wiki);

		bugSubmit = new JMenuItem("Submit a bug");
		helpMenu.add(bugSubmit);

		forkUs = new JMenuItem("Fork us on bitbucket");
		helpMenu.add(forkUs);

		helpMenu.add(new JSeparator());

		about = new JMenuItem("About Us");
		helpMenu.add(about);

		Event e = new Event();
		newGame.addActionListener(e);
		saveGame.addActionListener(e);
		loadGame.addActionListener(e);
		undo.addActionListener(e);
		statistics.addActionListener(e);
		exitGame.addActionListener(e);

		easyLevel.addActionListener(e);
		normalLevel.addActionListener(e);
		hardLevel.addActionListener(e);
		customLevel.addActionListener(e);
		soundToggle.addActionListener(e);

		wiki.addActionListener(e);
		bugSubmit.addActionListener(e);
		forkUs.addActionListener(e);
		about.addActionListener(e);

	}

	/**
	 * Make avilable undo item
	 * 
	 * @param avilable
	 */
	public void makeAvilableUndo(boolean avilable) {
		undo.setEnabled(avilable);
	}

	/**
	 * Switch to new game mode
	 */
	public void makeNewGameMode() {
		saveGame.setEnabled(true);
		loadGame.setEnabled(true);
		undo.setEnabled(false);
	}

	/**
	 * Switch to game over mode
	 */
	public void makeGameOverMode() {
		saveGame.setEnabled(false);
		loadGame.setEnabled(false);
		undo.setEnabled(false);
	}

	/**
	 * Listner for all items in this menubar
	 * 
	 * @author Mhmad
	 * 
	 */
	private class Event implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Object source = e.getSource();

			if (source == newGame) {
				newGame();
			} else if (source == saveGame)
				saveGame();
			else if (source == loadGame)
				loadGame();
			else if (source == undo) {
				mainGUI.undo();
				undo.setEnabled(false);
			} else if (source == statistics) {
				mainGUI.showStatistics();
			} else if (source == exitGame)
				exitGame();
			else if (source == easyLevel) {
				Settings.loadEasy();
				mainGUI.stopTimer();
				ColorsGroup.setColors(Settings.getColorTypes());
				mainGUI.intilizeComponent(Settings.getGridRows(),
						Settings.getGridCols());
			} else if (source == normalLevel) {
				mainGUI.stopTimer();
				Settings.loadNormal();
				ColorsGroup.setColors(Settings.getColorTypes());
				mainGUI.intilizeComponent(Settings.getGridRows(),
						Settings.getGridCols());
			} else if (source == hardLevel) {
				Settings.loadHard();
				mainGUI.stopTimer();
				ColorsGroup.setColors(Settings.getColorTypes());
				mainGUI.intilizeComponent(Settings.getGridRows(),
						Settings.getGridCols());
			} else if (source == customLevel) {
				mainGUI.newGameSettings();
			} else if (source == soundToggle) {
				mainGUI.toggleSound(soundToggle.isSelected());
			} else if (source == wiki)
				openWiki();
			else if (source == bugSubmit)
				submitABug();
			else if (source == forkUs)
				forkUsOnBitbucket();
			else
				about();
		}

		/**
		 * New Game
		 */
		private void newGame() {
			int chosie = JOptionPane.showConfirmDialog(null,
					"Are you sure You want new game ?");
			if (chosie == JOptionPane.YES_OPTION) {
				mainGUI.stopTimer();
				Settings.loadNormal();
				ColorsGroup.setColors(Settings.getColorTypes());
				mainGUI.intilizeComponent(Settings.getGridRows(),
						Settings.getGridCols());
			}
		}

		/**
		 * Save Game
		 */
		private void saveGame() {
			SaveLoadGame.saveGame(mainGUI.getGame(), "savedGame.sav");
		}

		/**
		 * Load game from file
		 */
		private void loadGame() {
			Game loadedGame = SaveLoadGame.loadGame("savedGame.sav");
			if (loadedGame != null) {
				mainGUI.setGame(loadedGame);
				mainGUI.getGame().setNeedUpdate(true);
			}
			// mainGUI.getGame().repeatAllTimer();
		}

		/**
		 * Close
		 */
		private void exitGame() {
			System.exit(0);
		}

		/**
		 * Opens wiki
		 */
		private void openWiki() {
			try {
				Desktop.getDesktop()
						.browse(new URI(
								"https://bitbucket.org/Mhmad_Alnajjar/break_bricks/wiki/Home"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"#ERROR : no browsers detected", "Error !",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		/**
		 * Opens a new tap in default browser to submit a new bug to the
		 * repository
		 */
		private void submitABug() {
			try {
				Desktop.getDesktop()
						.browse(new URI(
								"https://bitbucket.org/Mhmad_Alnajjar/break_bricks/issues/new"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"#ERROR : no browsers detected", "Error !",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		/**
		 * Opens a new tap in default browser to submit a new bug to the
		 * repository
		 */
		private void forkUsOnBitbucket() {
			try {
				Desktop.getDesktop()
						.browse(new URI(
								"https://bitbucket.org/Mhmad_Alnajjar/break_bricks/fork"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"#ERROR : no browsers detected", "Error !!",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		/**
		 * Show about us dialog
		 */
		private void about() {
			JOptionPane.showMessageDialog(null,
					"Programmed By : MAMProgr , Mhmad Al-Najjar , Ammar Lakis");
		}
	}
}
