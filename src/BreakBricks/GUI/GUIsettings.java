package BreakBricks.GUI;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import BreakBricks.Core.ColorsGroup;
import BreakBricks.Settings.GameSize;
import BreakBricks.Settings.Settings;

@SuppressWarnings("serial")
/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 *
 */
public class GUIsettings extends JPanel implements ActionListener,
		ChangeListener {

	/*
	 * The panel!
	 */
	private JPanel backgroundPanel = new JPanel();

	/*
	 * The panel!
	 */
	private JComboBox<String> gameTypeComboBox = new JComboBox<String>();

	/*
	 * Columns count
	 */
	private JLabel rowsLabel = new JLabel();
	private JSlider columnsSlider;
	/*
	 * Rows count
	 */
	private JLabel columnsLabel = new JLabel();
	private JSlider rowsSlider;

	/**
	 * Start button..
	 */
	private JButton startButton = new JButton();

	/**
	 * Main component
	 */
	private Component component = null;

	/*
	 * Constructor..
	 */
	public GUIsettings(Component component, GameSize gameSize) {
		super();
		this.component = component;
		setLayout(null);

		add(backgroundPanel);
		backgroundPanel.setSize(300, 200);
		backgroundPanel.setLayout(null);

		rowsLabel.setBounds(15 + 15, 38 + 55, 91, 14);
		rowsLabel.setText("Rows: \t 12");
		backgroundPanel.add(rowsLabel);

		columnsLabel.setBounds(15 + 15, 75 + 55, 91, 14);
		columnsLabel.setText("Columns: \t 12");
		backgroundPanel.add(columnsLabel);

		rowsSlider = new JSlider();
		rowsSlider.setBounds(113 + 15, 36 + 55, 140, 23);
		backgroundPanel.add(rowsSlider);
		rowsSlider.addChangeListener(this);
		rowsSlider.setValue(12);
		rowsSlider.setMaximum(gameSize.getMaxRows());
		rowsSlider.setMinimum(gameSize.getMinRows());

		columnsSlider = new JSlider();
		columnsSlider.setBounds(113 + 15, 73 + 55, 140, 23);
		backgroundPanel.add(columnsSlider);
		columnsSlider.addChangeListener(this);
		columnsSlider.setValue(12);
		columnsSlider.setMinimum(gameSize.getMinColumns());
		columnsSlider.setMaximum(gameSize.getMaxColumns());

		final JLabel gameModeLabel = new JLabel();
		gameModeLabel.setBounds(15 + 15, 5 + 55, 91, 14);
		gameModeLabel.setText("Game Level:");
		backgroundPanel.add(gameModeLabel);

		gameTypeComboBox.setBounds(113 + 15, 3 + 55, 140, 22);
		gameTypeComboBox.addActionListener(this);
		backgroundPanel.add(gameTypeComboBox);
		gameTypeComboBox.addItem("Easy");
		gameTypeComboBox.addItem("Normal");
		gameTypeComboBox.addItem("Hard");
		gameTypeComboBox.setSelectedIndex(1);

		startButton.setBounds(15 + 15, 122 + 55, 240, 23);
		startButton.setText("Start!");
		backgroundPanel.add(startButton);
		startButton.addActionListener(this);

	}

	/**
	 * Always place this JPanel in the middle of the super component
	 */
	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		if (width < backgroundPanel.getWidth() + 2 * x) {
			width = backgroundPanel.getWidth() + 2 * x;
		}
		if (height < backgroundPanel.getHeight() + 2 * y) {
			height = backgroundPanel.getHeight() + 2 * y;
		}
		backgroundPanel.setLocation((width - backgroundPanel.getWidth()) / 2,
				(height - backgroundPanel.getHeight()) / 2);
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		if (arg0.getSource() == rowsSlider) {
			rowsLabel.setText("Rows: \t" + rowsSlider.getValue());
		} else if (arg0.getSource() == columnsSlider) {
			columnsLabel.setText("Columns: \t" + columnsSlider.getValue());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == gameTypeComboBox) {
			if (gameTypeComboBox.getSelectedItem() == "Easy") {
				Settings.loadEasy();
			} else if (gameTypeComboBox.getSelectedItem() == "Normal") {
				Settings.loadNormal();
			} else if (gameTypeComboBox.getSelectedItem() == "Hard") {
				Settings.loadHard();
			}
			rowsSlider.setValue(Settings.getGridRows());
			columnsSlider.setValue(Settings.getGridCols());
		} else {
			ColorsGroup.setColors(Settings.getColorTypes());
			((MainGUI) this.component).intilizeComponent(rowsSlider.getValue(),
					columnsSlider.getValue());
		}
	}
}
