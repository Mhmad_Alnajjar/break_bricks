/**
 * 
 */
package BreakBricks.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import BreakBricks.Statistics.Statistics;

/**
 * @author Mhmad
 * 
 */
@SuppressWarnings("serial")
/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 *
 */
public class GUIStatistics extends JPanel {
	/**
	 * Table That Hold The Statistics
	 */
	private JTable table;

	/**
	 * Statistics
	 */
	private Statistics statistics;

	/**
	 * Constructor
	 * 
	 * @param statistics
	 *            load from it top player
	 */
	public GUIStatistics(Statistics statistics) {
		this.statistics = statistics;
		this.statistics.loadStatistics();

		setLayout(new BorderLayout());

		String[] columnName = { "Name", "Score" };

		ArrayList<String[]> temp = this.statistics.getTopTenPlayer();

		Object[][] data = new Object[temp.size()][2];

		for (int i = 0; i < temp.size(); i++)
			for (int j = 0; j < 2; j++)
				data[i][j] = temp.get(i)[j];

		table = new JTable(data, columnName);
		table.setPreferredScrollableViewportSize(new Dimension(500, 160));
		table.setFillsViewportHeight(true);
		table.setEnabled(false);

		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane, BorderLayout.CENTER);
	}
}
