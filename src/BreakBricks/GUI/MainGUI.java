package BreakBricks.GUI;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import BreakBricks.Core.ColorsGroup;
import BreakBricks.Core.Game;
import BreakBricks.Settings.GameSize;
import BreakBricks.Settings.Settings;
import BreakBricks.Settings.Sound;
import BreakBricks.Statistics.Statistics;

@SuppressWarnings("serial")
/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 *
 */
public class MainGUI extends JFrame {
	/**
	 * Logic Game
	 */
	private Game game;

	/**
	 * Menu bar
	 */
	private GUImenu menuBar;

	/**
	 * Panel holds the grid of cell
	 */
	private GUIgrid grid;

	/**
	 * Panel for score and time
	 */
	private JPanel infoPanel = new JPanel();

	/**
	 * Score Label
	 */
	private JLabel scoreLabel = new JLabel();

	/**
	 * Time Label
	 */
	private JLabel timeLabel = new JLabel();

	/**
	 * Statistics for best ten player
	 */
	private Statistics statistics;

	/**
	 * Generator of Special Cell
	 */
	private Timer genrator;

	private GUIsettings settingsGUI;

	private Sound backSound;

	/**
	 * Constructor
	 * 
	 * @param title
	 *            Frame title
	 * @param width
	 *            Frame width
	 * @param height
	 *            Frame hieght
	 * @param iconFileName
	 *            Frame Icon File Name
	 * @param resizable
	 *            Is resizable
	 * @param centered
	 *            Is centered
	 */
	public MainGUI(String title, int width, int height, String iconFileName,
			boolean resizable, boolean centered) {
		super(title);
		if (iconFileName != null) {
			setIcon(iconFileName);
		}
		setSize(width, height);
		setResizable(resizable);
		if (centered == true) {
			setCentered();
		}

		backSound = new Sound("BackSound");

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		menuBar = new GUImenu(this);
		setJMenuBar(menuBar);

		statistics = new Statistics();
		statistics.loadStatistics();

		// newGameSettings();
		Settings.loadNormal();
		ColorsGroup.setColors(Settings.getColorTypes());
		intilizeComponent(Settings.getGridRows(), Settings.getGridCols());

		setVisible(true);
	}

	/**
	 * To Generete setting panel
	 */
	void newGameSettings() {
		game = null;
		stopTimer();
		menuBar.makeGameOverMode();
		settingsGUI = new GUIsettings(this, new GameSize(5, 25, 5, 25));
		settingsGUI.setVisible(false);
		setContentPane(settingsGUI);
		settingsGUI.setVisible(true);
		repaint();

	}

	/**
	 * Intilize cell grid
	 * 
	 * @param rows
	 *            Number of rows
	 * @param cols
	 *            Number of cols
	 */
	void intilizeComponent(int rows, int cols) {
		this.game = new Game(rows, cols);

		GenerateTimer gt = new GenerateTimer(Settings.getSCFrequency(), rows,
				cols);

		this.genrator = new Timer(1000, gt);
		this.genrator.start();

		JPanel newContentPane = new JPanel();
		newContentPane.setVisible(false);

		this.setContentPane(newContentPane);
		newContentPane.setVisible(true);

		setLayout(new BorderLayout());

		getContentPane().add(infoPanel, BorderLayout.NORTH);
		infoPanel.setSize(60, 60);

		infoPanel.setLayout(new BorderLayout());

		infoPanel.add(scoreLabel, BorderLayout.WEST);

		infoPanel.add(timeLabel, BorderLayout.EAST);
		scoreLabel.setText("Score: 0");
		timeLabel.setText("Time: 0");

		// ColorsGroup.setColors();

		grid = new GUIgrid(this, game.getColorGrid(), rows, cols);

		this.getContentPane().add(grid, BorderLayout.CENTER);

		menuBar.makeNewGameMode();
	}

	/**
	 * To generete Statistics pane
	 */
	void showStatistics() {
		JOptionPane.showMessageDialog(null, new GUIStatistics(statistics));
	}

	/**
	 * Make new game
	 */
	public void newGame() {
		game.newGame();
		this.grid.updateColorGrid(game.getColorGrid());
		makeNewGameMode();
		scoreLabel.setText("Score: " + game.getPoints());
	}

	/**
	 * Click a cell
	 * 
	 * @param row
	 *            row num
	 * @param col
	 *            col num
	 */
	public void click(int row, int col) {
		if (isEnabled())
			if (game.checkCell(row, col)) {
				if (game.destroyCell(row, col)) {
					new Sound("Explosive");
					this.grid.updateColorGrid(game.getColorGrid());
					scoreLabel.setText("Scrore : " + game.getPoints());
					menuBar.makeAvilableUndo(true);
					this.grid.updateMarkedGrid(game.getMarkedGrid());
					repaint();
					if (game.isSolvable() == false) {
						new Sound("GameOver");
						stopTimer();
						if (statistics.tryInsertPlayer(game.getPoints())) {
							String name = JOptionPane.showInputDialog(null,
									"Your Score Is High Enter Your Name :)");
							if (name != null && name != "") {
								statistics.insertPlayer(name, game.getPoints());
								statistics.saveStatistics();
							}
						}
						int choise = JOptionPane.showConfirmDialog(null,
								"Your Score is : " + game.getPoints()
										+ "\n Do you want to play again ?",
								"Game Over", JOptionPane.NO_OPTION);
						if (choise == JOptionPane.YES_OPTION)
							newGame();
						else
							makeGameOverMode();
					}
				}
			} else {
				new Sound("Wrong");
			}
	}

	/**
	 * Mouse over cell
	 * 
	 * @param row
	 *            row num
	 * @param col
	 *            col num
	 */
	public void over(int row, int col) {
		if (isEnabled())
			if (game.checkCell(row, col)) {
				this.grid.updateMarkedGrid(game.getMarkedGrid());
				repaint();
			}
	}

	/**
	 * Make new game mode
	 */
	public void makeNewGameMode() {
		if (genrator != null && genrator.isRunning() == false)
			genrator.start();
		grid.setEnabled(true);
		menuBar.makeNewGameMode();
	}

	/**
	 * Make game over mode
	 */
	public void makeGameOverMode() {
		stopTimer();
		grid.setEnabled(false);
		menuBar.makeGameOverMode();
	}

	/**
	 * Undo last step
	 */
	public void undo() {
		if (game.restoreGame() == true) {
			this.grid.updateColorGrid(game.getColorGrid());
			scoreLabel.setText("Scrore : " + game.getPoints());
		}
	}

	/**
	 * Sets the icon of the JFrame
	 * 
	 * @param filename
	 *            the path and name of the image to use as the icon for the
	 *            JFrame
	 */
	protected void setIcon(String filename) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("/images/" + filename)));
	}

	/**
	 * Centers the window on the screen
	 */
	protected void setCentered() {
		setLocation(
				(Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);
	}

	public void stopTimer() {
		if (genrator != null)
			genrator.stop();
	}

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * @param game
	 *            the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * Timer that generate Special cells
	 * 
	 * @author Mhmad
	 * 
	 */
	public class GenerateTimer implements ActionListener {
		/**
		 * Current time
		 */
		private int counter = 0;

		/**
		 * Special cell frequency
		 */
		private int specialShowTime = 0;

		/**
		 * Rows number
		 */
		private int row;

		/**
		 * Columns number
		 */
		private int col;

		/**
		 * Constructor
		 * 
		 * @param specialShowTime
		 *            Special cell frequency
		 * @param row
		 *            Rows number
		 * @param col
		 *            Cols number
		 */
		public GenerateTimer(int specialShowTime, int row, int col) {
			this.specialShowTime = specialShowTime;
			this.row = row;
			this.col = col;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			counter++;

			timeLabel.setText("Time : " + counter);
			if (counter % specialShowTime == 0) {// Time To genrate Special Cell
				int randRow = (int) (Math.random() * (this.row));
				int randCol = (int) (Math.random() * (this.col));

				if ((int) (Math.random() * 2) == 1)
					MainGUI.this.game.makeExplosive(randRow, randCol);
				else
					MainGUI.this.game.makeTransparent(randRow, randCol);

				MainGUI.this.grid.updateColorGrid(MainGUI.this.game
						.getColorGrid());
				repaint();
				MainGUI.this.game.setNeedUpdate(false);
			}

			if (MainGUI.this.game.isNeedUpdate() == true) {
				MainGUI.this.grid.updateColorGrid(MainGUI.this.game
						.getColorGrid());
				repaint();
				MainGUI.this.game.setNeedUpdate(false);
			}
		}
	}

	public void toggleSound(boolean isSelected) {
		if (isSelected)
			backSound.turnOff();
		else
			backSound.turnOn();
	}
}
