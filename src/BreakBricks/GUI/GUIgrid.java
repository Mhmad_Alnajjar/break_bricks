package BreakBricks.GUI;

import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JPanel;

@SuppressWarnings("serial")
/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 *
 */
public class GUIgrid extends JPanel {
	/**
	 * Main Panel that holds this grid
	 */
	private MainGUI mainGUI;

	/**
	 * cell Color grid
	 */
	private Integer[][] colorGrid;

	/**
	 * Rows Number
	 */
	private int rowNum;

	/**
	 * Columns Number
	 */
	private int colNum;

	/**
	 * Panel Layout
	 */
	private GridLayout layout;

	/**
	 * Cell Array holds all cell
	 */
	private GUIcell[][] cells;

	/**
	 * Constructor
	 * 
	 * @param mainGUI
	 *            Panel that holds this grid
	 * @param colorGrid
	 *            Cell color array
	 * @param rowNum
	 *            Rows number
	 * @param colNum
	 *            Columns number
	 */
	public GUIgrid(MainGUI mainGUI, Integer[][] colorGrid, int rowNum,
			int colNum) {
		this.setMainGUI(mainGUI);
		this.colorGrid = colorGrid;
		setRowNum(rowNum);
		setColNum(colNum);

		layout = new GridLayout(rowNum, colNum);

		setLayout(layout);

		cells = new GUIcell[getRowNum()][getColNum()];

		for (int i = 0; i < rowNum; i++) {
			for (int j = 0; j < colNum; j++) {
				cells[i][j] = new GUIcell(this, i, j, this.colorGrid[i][j]);
				add(cells[i][j]);
			}
		}

		// Rresize..
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {

				int newWidth = cells[0][0].getWidth();
				int newHeight = cells[0][0].getHeight();
				GUIcell.resized(newWidth, newHeight);
			}
		});

		setSize(100, 100);
	}

	/**
	 * Set columns number
	 * 
	 * @param colNum
	 *            column number
	 */
	public void setColNum(int colNum) {
		if (colNum > 0)
			this.colNum = colNum;
		else
			this.colNum = 4;
	}

	/**
	 * Get columns number
	 * 
	 * @return columns number
	 */
	public int getColNum() {
		return this.colNum;
	}

	/**
	 * Set Rows number
	 * 
	 * @param rowNum
	 *            Rows number
	 */
	public void setRowNum(int rowNum) {
		if (rowNum > 0)
			this.rowNum = rowNum;
		else
			this.rowNum = 4;
	}

	/**
	 * Get Rows number
	 * 
	 * @return Rows number
	 */
	public int getRowNum() {
		return this.rowNum;
	}

	/**
	 * @return the mainGUI
	 */
	public MainGUI getMainGUI() {
		return mainGUI;
	}

	/**
	 * @param mainGUI
	 *            the mainGUI to set
	 */
	public void setMainGUI(MainGUI mainGUI) {
		this.mainGUI = mainGUI;
	}

	/**
	 * Colors grid
	 * 
	 * @param colorGrid
	 */
	public void updateColorGrid(Integer[][] colorGrid) {
		this.colorGrid = colorGrid;

		for (int i = 0; i < rowNum; i++)
			for (int j = 0; j < colNum; j++)
				cells[i][j].setColor(this.colorGrid[i][j]);
	}

	/**
	 * marked grid
	 * 
	 * @param markedGrid
	 */
	public void updateMarkedGrid(boolean[][] markedGrid) {
		for (int i = 0; i < rowNum; i++)
			for (int j = 0; j < colNum; j++)
				cells[i][j].setMarked(markedGrid[i][j]);
	}
}
