package BreakBricks.Core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import BreakBricks.Settings.Settings;

/**
 * 
 * @author Mhmad al-Najjar ,MAMProgr(Mohammed Anas Al-Mahdi), Ammar lakis.
 * 
 */
public class ExplosiveCell extends Cell {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3698725266181641235L;

	/**
	 * Time To Live
	 */
	private static int ttl = 3;

	/**
	 * Timer
	 */
	private Timer timer;

	/**
	 * Create a Explosive Cell on a specific position and random color
	 * 
	 * @param grid
	 *            grid
	 * @param row
	 *            Row of Cell
	 * @param col
	 *            Column of Cell
	 */
	public ExplosiveCell(Grid grid, int row, int col) {
		super(grid, row, col);
		ttl = Settings.getSCShowTime();
		TimeToLive time = new TimeToLive();

		timer = new Timer(1000, time);
		timer.start();
	}

	/**
	 * Create a Explosive Cell on a specific position and color
	 * 
	 * @param row
	 *            Row of Cell
	 * @param col
	 *            Column of Cell
	 * @param colorIndex
	 *            color index for new Cell, if this colorIndex is not valid a
	 *            random color is used
	 */
	public ExplosiveCell(Grid grid, int row, int col, int colorIndex) {
		super(grid, row, col, colorIndex);

		turnOntimer();
	}

	/**
	 * Turn on Timer..
	 */
	void turnOntimer() {
		TimeToLive time = new TimeToLive();

		timer = new Timer(1000, time);
		timer.start();
	}

	/**
	 * Find Cells with the same color-index which are next to each other
	 */
	@Override
	public void findsame() {
		int row = this.getRow();
		int col = this.getCol();

		if (grid.isEmpty(row, col + 1) == false
				&& grid.getCell(row, col + 1).getColorIndex() == getColorIndex())
			bomb();
		else if (grid.isEmpty(row, col - 1) == false
				&& grid.getCell(row, col - 1).getColorIndex() == getColorIndex())
			bomb();
		else if (grid.isEmpty(row + 1, col) == false
				&& grid.getCell(row + 1, col).getColorIndex() == getColorIndex())
			bomb();
		else if (grid.isEmpty(row - 1, col) == false
				&& grid.getCell(row - 1, col).getColorIndex() == getColorIndex())
			bomb();
	}

	/**
	 * Bomb The Cell :)
	 */
	private void bomb() {
		setMark(true);
		timer.stop();

		int row = this.getRow();
		int col = this.getCol();

		if (grid.isEmpty(row, col + 1) == false)
			grid.getCell(row, col + 1).setMark(true);
		if (grid.isEmpty(row, col - 1) == false)
			grid.getCell(row, col - 1).setMark(true);
		if (grid.isEmpty(row + 1, col) == false)
			grid.getCell(row + 1, col).setMark(true);
		if (grid.isEmpty(row - 1, col) == false)
			grid.getCell(row - 1, col).setMark(true);

		if (grid.isEmpty(row + 1, col + 1) == false)
			grid.getCell(row + 1, col + 1).setMark(true);
		if (grid.isEmpty(row - 1, col - 1) == false)
			grid.getCell(row - 1, col - 1).setMark(true);
		if (grid.isEmpty(row + 1, col - 1) == false)
			grid.getCell(row + 1, col - 1).setMark(true);
		if (grid.isEmpty(row - 1, col + 1) == false)
			grid.getCell(row - 1, col + 1).setMark(true);

		grid.marked = 9;
	}

	/**
	 * this class Take Care Of the time remains to live
	 * 
	 * @author Mhmad
	 * 
	 */
	public class TimeToLive implements ActionListener {
		public int counter = 0;

		public TimeToLive() {
			counter = ttl;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (counter < 0) {
				timer.stop();
				grid.getGame().setNeedUpdate(true);
				grid.newCell(ExplosiveCell.this.getRow(),
						ExplosiveCell.this.getCol(),
						ExplosiveCell.this.getColorIndex());
			}
			counter--;
		}
	}

	/**
	 * @return the timer
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * @param timer
	 *            the timer to set
	 */
	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
