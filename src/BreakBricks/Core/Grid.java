/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BreakBricks.Core;

import java.io.Serializable;

/**
 * Main game grid
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 */
public class Grid implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * columns of the matrix
	 */
	private int cols;

	/**
	 * rows of the matrix
	 */
	private int rows;

	/**
	 * the grid
	 */
	private Cell[][] grid;

	/**
	 * The number of marked Cells
	 */
	int marked = 0;

	/**
	 * The Game That Grid Belong
	 */
	private Game game;

	/**
	 * Creates new empty grid
	 * 
	 * @param rows
	 *            row count
	 * @param cols
	 *            column count
	 */
	public Grid(Game game, int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		this.marked = 0;
		this.setGame(game);

		grid = new Cell[rows][cols];
	}

	/**
	 * Returns the column-count of the grid
	 * 
	 * @return gridcolumn count
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Returns the row-count of the grid
	 * 
	 * @return grid row count
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Creates a new Cell on the grid at position x,y with a special color
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @param colorIndex
	 *            color-index
	 * @return true if Cell created, false otherwise
	 */
	boolean newCell(int x, int y, int colorIndex) {
		if (x >= 0 && y >= 0 && x < getRows() && y < getCols()) {
			grid[x][y] = new Cell(this, x, y, colorIndex);
			return true;
		}
		return false;
	}

	/**
	 * Change Cell color on the grid at position x,y
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @param colorIndex
	 *            new color-index
	 * @return true if Cell changed, false otherwise
	 */
	boolean changeCell(int x, int y, int colorIndex) {
		if (x >= 0 && y >= 0 && x < getRows() && y < getCols()) {
			grid[x][y].moveTo(colorIndex);
			return true;
		}
		return false;
	}

	/**
	 * Creates a new Cell on the grid at position x,y with a random color
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return true if Cell created, false otherwise
	 */
	boolean newCell(int x, int y) {
		if (x >= 0 && y >= 0 && x < getRows() && y < getCols()) {
			grid[x][y] = new Cell(this, x, y);
			return true;
		}
		return false;
	}

	/**
	 * Creates a new Explosive Cell on the grid at position x,y with a random
	 * color
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return true if Cell created, false otherwise
	 */
	boolean newExplosiveCell(int x, int y) {
		if (x >= 0 && y >= 0 && x < getRows() && y < getCols()
				&& this.isEmpty(x, y) == false) {
			grid[x][y] = new ExplosiveCell(this, x, y,
					grid[x][y].getColorIndex());
			return true;
		}
		return false;
	}

	/**
	 * Creates a new Transparent Cell on the grid at position x,y with a random
	 * color
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return true if Cell created, false otherwise
	 */
	public boolean newTransparentCell(int x, int y) {
		if (x >= 0 && y >= 0 && x < getRows() && y < getCols()
				&& this.isEmpty(x, y) == false) {
			grid[x][y] = new TransparentCell(this, x, y,
					grid[x][y].getColorIndex());
			return true;
		}
		return false;
	}

	/**
	 * Returns the Cell at a specific position in the grid
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return Cell-reference, null if this Cell doesn't exist
	 */
	Cell getCell(int x, int y) {
		if (isEmpty(x, y) == false) {
			return grid[x][y];
		} else {
			return null;
		}
	}

	/**
	 * Checks if at a specific position a Cell exists
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return exists a Cell at x,y?
	 */
	public boolean isEmpty(int x, int y) {
		if (x < 0 || y < 0 || x >= getRows() || y >= getCols()) {
			return true;
		}
		return (grid[x][y] == null);
	}

	/**
	 * Moves a specific Cell to a new location in the grid, regardless if on the
	 * destination there is a Cell
	 * 
	 * @param x
	 *            old row-index
	 * @param y
	 *            old column-index
	 * @param toX
	 *            new row-index 0 <= toX < rows, x != toX
	 * @param toY
	 *            new column-index 0 <= toY < rows, y != toY
	 */
	public void moveTo(int x, int y, int toX, int toY) {
		if (x != toX || y != toY) {
			grid[toX][toY] = grid[x][y];
			if (isEmpty(toX, toY) == false) {
				grid[toX][toY].moveTo(toX, toY);
			}
			grid[x][y] = null;
		}
	}

	/**
	 * Removes a Cell on a specific location from the grid
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 */
	void removeCell(int x, int y) {
		if (isEmpty(x, y) == false) {
			grid[x][y] = null;
		}
	}

	/**
	 * Mark a specific Cell
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @param is
	 *            set mark to setTo
	 */
	public void setMark(int x, int y, boolean is) {
		if (isEmpty(x, y) == false) {
			grid[x][y].setMark(is);
		}
	}

	/**
	 * Checks if at a specific position a Cell is marked
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return is marked?, false if Cell doesn't exist
	 */
	public boolean isMarked(int x, int y) {
		if (isEmpty(x, y) == true) {
			return false;
		}
		return grid[x][y].isMarked();
	}

	/**
	 * Unmarks all Cells on the Grid
	 */
	public void unMarkAll() {
		marked = 0;
		int row = getRows() - 1;
		int col = getCols() - 1;
		while (col >= 0 && isEmpty(row, col) == false) {
			while (row >= 0 && isEmpty(row, col) == false) {
				setMark(row, col, false);
				row--;
			}
			col--;
			row = rows - 1;
		}
	}

	/**
	 * Returns the color-index of a Cell at a specific position in the grid
	 * 
	 * @param x
	 *            row-index
	 * @param y
	 *            column-index
	 * @return Cell-Color-index, -1 if this Cell doesn't exist
	 */
	public int getColor(int x, int y) {
		if (isEmpty(x, y) == true) {
			return -1;
		} else {
			return grid[x][y].getColorIndex();
		}
	}

	public boolean returnDefault(int row, int col) {
		if (this.isEmpty(row, col) == false) {
			Integer colorIndex = getCell(row, col).getColorIndex();
			newCell(row, col, colorIndex);
			return true;
		}
		return false;
	}

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * @param game
	 *            the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}
}
