/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BreakBricks.Core;

import java.io.Serializable;

/**
 * the Cell
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 */
public class Cell implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6030126375917685023L;

	/**
	 * The Parent matrix of Cells
	 */
	protected Grid grid;

	/**
	 * columns of the matrix
	 */
	private int col;

	/**
	 * rows of the matrix
	 */
	private int row;

	/**
	 * color-index of this Cell
	 */
	private int colorIndex;

	/**
	 * is this Cell marked
	 */
	private boolean marked = false;

	/**
	 * Creates a Cell with a specific position and color
	 * 
	 * @param row
	 *            Horizontal index of cell
	 * @param col
	 *            Vertical index of Cell
	 * @param colorIndex
	 *            Color index for new Cell. will replace invalid values with
	 *            random color.
	 */
	public Cell(Grid grid, int row, int col, int colorIndex) {
		this.grid = grid;
		moveTo(row, col);
		if (colorIndex >= 0 && colorIndex < ColorsGroup.getColorsCount())
			this.colorIndex = colorIndex;
		else
			this.colorIndex = (int) (Math.random()
					* (ColorsGroup.getColorsCount() - 1) + 1);
	}

	/**
	 * Creates a Cell with a specific position and color
	 * 
	 * @param grid
	 *            The parent grid
	 * @param row
	 *            Horizontal index of cell
	 * @param col
	 *            Vertical index of Cell
	 */
	public Cell(Grid grid, int row, int col) {
		this.grid = grid;
		moveTo(row, col);
		this.colorIndex = (int) (Math.random()
				* (ColorsGroup.getColorsCount() - 1) + 1);
	}

	/**
	 * Returns the row index of this Cell
	 * 
	 * @return Row
	 */
	final public int getRow() {
		return row;
	}

	/**
	 * Returns the column index of this Cell
	 * 
	 * @return Col
	 */
	final public int getCol() {
		return col;
	}

	/**
	 * Returns the color index of this Cell
	 * 
	 * @return colorIndex The cell's color index.
	 */
	public int getColorIndex() {
		return colorIndex;
	}

	/**
	 * Changes the position of this cell
	 * 
	 * @param row
	 *            row-index
	 * @param col
	 *            column-index
	 */
	final public void moveTo(int row, int col) {
		this.row = row;
		this.col = col;
	}

	/**
	 * Changes the color of this cell
	 * 
	 * @param colorIndex
	 */
	final public void moveTo(int colorIndex) {
		this.colorIndex = colorIndex;
	}

	/**
	 * Toggles the mark status of this Cell
	 * 
	 * @param is
	 *            Mark active
	 */
	final public void setMark(boolean is) {
		if (marked != is) {
			marked = is;
		}
	}

	/**
	 * Returns whether if this Cell is marked or not
	 * 
	 * @return True if marked
	 */
	final public boolean isMarked() {
		return marked;
	}

	/**
	 * Find (recursively) all Cells with the same color-index which are next to
	 * each other
	 */
	public void findsame() {
		if (grid.isEmpty(row, col)) {
			return;
		}
		grid.marked++;
		grid.setMark(row, col, true);
		int color = grid.getColor(row, col);

		if (grid.getCell(row, col + 1) != null
				&& grid.getCell(row, col + 1).getColorIndex() == color
				&& !grid.isMarked(row, col + 1))
			grid.getCell(row, col + 1).findsame();
		if (grid.getCell(row, col - 1) != null
				&& grid.getCell(row, col - 1).getColorIndex() == color
				&& !grid.isMarked(row, col - 1))
			grid.getCell(row, col - 1).findsame();
		if (grid.getCell(row + 1, col) != null
				&& grid.getCell(row + 1, col).getColorIndex() == color
				&& !grid.isMarked(row + 1, col))
			grid.getCell(row + 1, col).findsame();
		if (grid.getCell(row - 1, col) != null
				&& grid.getCell(row - 1, col).getColorIndex() == color
				&& !grid.isMarked(row - 1, col))
			grid.getCell(row - 1, col).findsame();
	}
}
