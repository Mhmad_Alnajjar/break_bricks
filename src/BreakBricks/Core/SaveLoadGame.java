/**
 * 
 */
package BreakBricks.Core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 * 
 */
public class SaveLoadGame {

	/**
	 * Game we want to save in file
	 * 
	 * @param game
	 * @param path
	 * @return True if the saving process has accomplished successfully
	 */
	static public boolean saveGame(Game game, String path) {

		File savedFile = new File(path);

		try {
			FileOutputStream saveFile = new FileOutputStream(savedFile);
			// ObjectOutputStream save;
			try {
				ObjectOutputStream save = new ObjectOutputStream(saveFile);
				save.writeObject(game);
				save.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			try {
				savedFile.createNewFile();
				saveGame(game, path);
			} catch (IOException ee) {
				ee.printStackTrace();
			}
		}

		return false;
	}

	/**
	 * load saved game..
	 * 
	 * @param path
	 * @return Game loaded game
	 */
	static public Game loadGame(String path) {
		try {
			FileInputStream saveFile = new FileInputStream(path);
			ObjectInputStream restore = new ObjectInputStream(saveFile);

			Game restoredGame = (Game) restore.readObject();
			restore.close();

			return restoredGame;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
