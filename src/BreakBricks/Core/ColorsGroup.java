/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BreakBricks.Core;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 */
public class ColorsGroup {

	/*
	 * default ColorsGroup array
	 */
	private final static String[] defaultColors = { "Transparent", "Green",
			"Blue", "Purble", "Red", "Yellow" };

	/*
	 * ColorsGroup array
	 */
	private static String[] colors;

	/*
	 * ColorsGroup array (images)
	 */
	private static Image[] images;

	/*
	 * ColorsGroup array (explosive images)
	 */
	private static Image[] explosiveImages;

	/*
	 * images path
	 */
	private static String path = "/res/images/";

	/**
	 * default constructor..
	 */
	public ColorsGroup() {
		setColors();
	}

	/**
	 * set default colors..
	 */
	public static void setColors() {
		colors = defaultColors;

		fetchColors(colors);
	}

	/**
	 * set Colors
	 * 
	 * @param colorsString
	 */
	public static void setColors(String colorsString) {

		if (colorsString == null) {
			setColors();
			return;
		}

		colors = colorsString.split(",");

		fetchColors(colors);
	}

	/**
	 * fetch colors files..
	 * 
	 * @param colors
	 */
	public static void fetchColors(String[] colors) {

		if (colors.length < 1)
			setColors();
		else {
			ColorsGroup.colors = colors;

			images = new Image[colors.length];
			explosiveImages = new Image[colors.length];

			try {
				images[0] = ImageIO.read(ColorsGroup.class.getClass()
						.getResource(path + colors[0] + ".png"));
				explosiveImages[0] = images[0];
			} catch (IOException e) {
				setColors();
			}

			for (int i = 1; i < colors.length; i++) {
				try {
					images[i] = ImageIO.read(ColorsGroup.class.getClass()
							.getResource(path + colors[i] + ".png"));
					explosiveImages[i] = ImageIO.read(ColorsGroup.class
							.getClass().getResource(
									path + colors[i] + "Explosive" + ".png"));
				} catch (IOException e) {

					setColors();

				}
			}
		}
	}

	/**
	 * set Colors with specific dimensions..
	 * 
	 * @param colorsString
	 * @param width
	 * @param hieght
	 */
	public static void setColors(String colorsString, int width, int hieght) {
		// setColors(colorsString);
		if (colorsString != null)
			setColors(colorsString);

		fetchColors(colors);

		for (int i = 0; i < images.length; i++) {
			images[i] = images[i].getScaledInstance(width, hieght,
					Image.SCALE_SMOOTH);
			explosiveImages[i] = explosiveImages[i].getScaledInstance(width,
					hieght, Image.SCALE_SMOOTH);
		}
	}

	/**
	 * Returns the colors count
	 * 
	 * @return the colors array count
	 */
	public static int getColorsCount() {
		if (colors == null)
			setColors();
		return colors.length;
	}

	/**
	 * implode colors..
	 * 
	 * @return colors string
	 */
	public static String getColors() {
		if (colors == null)
			setColors();

		StringBuffer colorsString = new StringBuffer();

		for (int i = 0; i < colors.length - 1; i++)
			colorsString.append(colors[i] + ",");

		colorsString.append(colors[colors.length - 1]);

		return colorsString.toString();
	}

	/**
	 * Returns the image with color index from the colors array
	 * 
	 * @param color
	 * @return the specific image
	 */
	public static Image getImage(int color) {
		if (colors == null)
			setColors();
		if (color > 0)
			return images[color];
		else
			return explosiveImages[-color];
	}
}
