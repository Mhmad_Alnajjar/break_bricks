/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BreakBricks.Core;

import java.io.Serializable;

/**
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 */
public class Game implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3315624778212425278L;

	/**
	 * The matrix of Cells
	 */
	private Grid grid;

	/**
	 * Stores the grid, the matrix of Cell at the backup-point
	 */
	public Integer[][] gridOld;

	/**
	 * The points the user gained
	 */
	private int points = 0;

	/**
	 * The points the user gained at the backup-point
	 */
	private int oldPoints = 0;

	/**
	 * True if a first action was taken
	 */
	private boolean firstMove = true;

	/**
	 * True if a first action was taken
	 */
	private boolean isRestourable = false;

	/**
	 * To Konw If Update Needed
	 */
	private boolean needUpdate = true;

	/**
	 * Prepares a new Grid
	 * 
	 * @param rows
	 *            of the matrix
	 * @param cols
	 *            of the matrix
	 */
	public Game(int rows, int cols) {
		grid = new Grid(this, rows, cols);
		gridOld = new Integer[rows][cols];

		newGame();
	}

	/**
	 * Creates a new game. Resets all values and creates a new Cell grid
	 */
	public final void newGame() {
		points = 0;
		grid.marked = 0;
		firstMove = true;

		removeAllCellsFromGrid();
		fillGrid();
		if (isSolvable() == false) {
			newGame();
		}
	}

	/**
	 * Removes all Cells from grid
	 */
	private void removeAllCellsFromGrid() {
		int row = grid.getRows() - 1;
		int col = grid.getCols() - 1;
		while (col >= 0 && grid.isEmpty(row, col) == false) {
			while (row >= 0 && grid.isEmpty(row, col) == false) {
				removeCell(row, col);
				row--;
			}
			col--;
			row = grid.getRows() - 1;
		}
	}

	/**
	 * Removes a special Cell
	 * 
	 * @param row
	 *            row-index
	 * @param col
	 *            column-index
	 */
	final private void removeCell(int row, int col) {
		if (grid.isEmpty(row, col) == false) {
			grid.removeCell(row, col);
		}
	}

	/**
	 * fills the grid with Cells
	 */
	private void fillGrid() {
		for (int i = 0; i < grid.getRows(); i++) {
			for (int j = 0; j < grid.getCols(); j++) {
				newCell(i, j);
			}
		}
	}

	/**
	 * Creates a new Cell on the grid at position x,y if the position is empty
	 * 
	 * @param row
	 *            row-index
	 * @param col
	 *            column-index
	 */
	private boolean newCell(int row, int col) {
		if (grid.isEmpty(row, col) == true) {
			if (grid.newCell(row, col) == true) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates a new Cell on the grid at position x,y with a special color if
	 * the position is empty
	 * 
	 * @param row
	 *            row-index
	 * @param col
	 *            column-index
	 * @param colorIndex
	 *            color-index
	 */
	private boolean newCell(int row, int col, int colorIndex) {
		if (grid.isEmpty(row, col) == true) {
			if (grid.newCell(row, col, colorIndex) == true) {
				return true;
			}
		} else {
			if (grid.changeCell(row, col, colorIndex) == true) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns if the current game is solvable
	 * 
	 * @return check if game solvable
	 */
	public boolean isSolvable() {
		int row = grid.getRows() - 1;
		int col = grid.getCols() - 1;
		while (col >= 0 && grid.isEmpty(row, col) == false) {
			while (row >= 0 && grid.isEmpty(row, col) == false) {
				if (grid.isEmpty(row - 1, col) == false
						&& grid.getCell(row, col).getColorIndex() == grid
								.getCell(row - 1, col).getColorIndex()) {
					return true;
				}
				if (grid.isEmpty(row, col - 1) == false
						&& grid.getCell(row, col).getColorIndex() == grid
								.getCell(row, col - 1).getColorIndex()) {
					return true;
				}
				row--;
			}
			col--;
			row = grid.getRows() - 1;
		}
		return false;
	}

	/**
	 * Return the points of the current game
	 * 
	 * @return the points
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * save game
	 */
	public void saveGame() {
		isRestourable = true;
		oldPoints = points;
		int row = grid.getRows() - 1;
		int col = grid.getCols() - 1;
		while (col >= 0) {
			while (row >= 0) {
				if (grid.isEmpty(row, col) == false) {
					gridOld[row][col] = grid.getCell(row, col).getColorIndex();
				} else {
					gridOld[row][col] = null;
				}
				row--;
			}
			col--;
			row = grid.getRows() - 1;
		}
	}

	/**
	 * Restore saved game
	 */
	public boolean restoreGame() {
		if (isRestourable == true) {
			int row = grid.getRows() - 1;
			int col = grid.getCols() - 1;
			while (col >= 0) {
				while (row >= 0) {
					if (gridOld[row][col] != null) {
						newCell(row, col, gridOld[row][col]);
					}
					row--;
				}
				col--;
				row = grid.getRows() - 1;
			}
			points = oldPoints;
			isRestourable = false;
			return true;
		}
		return false;
	}

	/*
	 * Destroy Cell
	 */
	public boolean destroyCell(int row, int col) { // second step
		if (grid.isEmpty(row, col) == true)
			return false;
		if (firstMove) {
			firstMove = false;
		}
		saveGame();
		points += getCalculatedPoints();
		removeMarkedCells(row, col);
		generateNewCols();
		grid.marked = 0;

		if (grid.isEmpty(grid.getRows() - 1, grid.getCols() - 1) == true) {

		}
		if (isSolvable() == false) {
			// finished
			// !!--------------------------------------------------------------------------------------------------------------------
		}
		return true;
	}

	/*
	 * Check Cell
	 */
	public boolean checkCell(int row, int col) { // first step
		grid.unMarkAll();
		if (grid.getCell(row, col) != null) {
			grid.getCell(row, col).findsame();
			if (grid.marked == 1) {
				grid.unMarkAll();
				return false;
			}
		}
		return true;
	}

	/**
	 * Calculates the points
	 * 
	 * @return calculated points
	 */
	private int getCalculatedPoints() {
		return (grid.marked * (grid.marked - 1));
	}

	/**
	 * Remove all marked Cells
	 * 
	 * @param row
	 *            row-index of a marked Cell
	 * @param col
	 *            column-index of a marked Cell
	 */
	protected void removeMarkedCells(int row, int col) {
		// first of all delete Cells in col
		for (int k = 0; k < grid.getCols(); k++) {
			for (int i = 0; i < grid.getRows(); i++) {
				// System.out.println(i);
				if (grid.isMarked(i, k) == true) {
					removeCell(i, k);
					for (int j = i; j > 0; j--) {
						grid.moveTo(j - 1, k, j, k);
					}
				}
			}
		}
		// now remove empty columns
		int firstEmpty = -1;
		for (int k = 0; k < grid.getCols(); k++) {
			if (grid.isEmpty(grid.getRows() - 1, k) == true && firstEmpty == -1) {
				firstEmpty = k;
			}
		}

		for (int k = grid.getCols() - 1; k > 0; k--) {
			while (grid.isEmpty(grid.getRows() - 1, k) == true
					&& firstEmpty <= k) {
				for (int j = k; j > 0; j--) {
					for (int i = 0; i < grid.getRows(); i++) {
						grid.moveTo(i, j - 1, i, j);
					}
				}
				firstEmpty++;
			}
		}
	}

	/**
	 * generate new columns..
	 */
	public void generateNewCols() {
		int lastEmpty = 0;

		while (lastEmpty < grid.getCols()) {
			if (grid.isEmpty(grid.getRows() - 1, lastEmpty))
				lastEmpty++;
			else
				break;
		}

		if (lastEmpty != -1)
			for (int j = 0; j < lastEmpty; j++)
				for (int i = 0; i < grid.getRows(); i++)
					newCell(i, j);
	}

	/**
	 * get colors grid..
	 * 
	 * @return Integer[][]
	 */
	public Integer[][] getColorGrid() {
		Integer[][] colorGrid = new Integer[grid.getRows()][grid.getCols()];
		int row = grid.getRows() - 1;
		int col = grid.getCols() - 1;
		while (col >= 0) {
			while (row >= 0) {
				if (grid.isEmpty(row, col) == false) {
					if (grid.getCell(row, col) instanceof TransparentCell)
						colorGrid[row][col] = 0;
					else if (grid.getCell(row, col) instanceof ExplosiveCell)
						colorGrid[row][col] = -grid.getCell(row, col)
								.getColorIndex();
					else
						colorGrid[row][col] = grid.getCell(row, col)
								.getColorIndex();
				} else {
					colorGrid[row][col] = null;
				}
				row--;
			}
			col--;
			row = grid.getRows() - 1;
		}
		return colorGrid;
	}

	/**
	 * get marked cells grid
	 * 
	 * @return boolean[][]
	 */
	public boolean[][] getMarkedGrid() {
		boolean[][] markedGrid = new boolean[grid.getRows()][grid.getCols()];
		int row = grid.getRows() - 1;
		int col = grid.getCols() - 1;
		while (col >= 0) {
			while (row >= 0) {
				if (grid.isEmpty(row, col) == false) {
					markedGrid[row][col] = grid.getCell(row, col).isMarked();
				} else {
					markedGrid[row][col] = false;
				}
				row--;
			}
			col--;
			row = grid.getRows() - 1;
		}
		return markedGrid;
	}

	public void undo() {

	}

	/**
	 * do explosive
	 * 
	 * @param row
	 * @param col
	 */
	public void makeExplosive(int row, int col) {
		grid.newExplosiveCell(row, col);
		setNeedUpdate(true);
	}

	/**
	 * do transparent..
	 * 
	 * @param row
	 * @param col
	 */
	public void makeTransparent(int row, int col) {
		grid.newTransparentCell(row, col);
		setNeedUpdate(true);
	}

	/**
	 * @return the needUpdate
	 */
	public boolean isNeedUpdate() {
		return needUpdate;
	}

	/**
	 * @param needUpdate
	 *            the needUpdate to set
	 */
	public void setNeedUpdate(boolean needUpdate) {
		this.needUpdate = needUpdate;
	}

	/**
	 * Repeat all timer
	 */
	public void repeatAllTimer() {
		for (int i = 0; i < grid.getRows(); i++) {
			for (int j = 0; j < grid.getCols(); j++) {
				if (grid.isEmpty(i, j) == false) {
					Cell cell = grid.getCell(i, j);

					if (cell instanceof ExplosiveCell)
						((ExplosiveCell) cell).turnOntimer();
					else if (grid.getCell(i, j) instanceof TransparentCell)
						((TransparentCell) cell).turnOntimer();
				}

			}
		}
	}

}
