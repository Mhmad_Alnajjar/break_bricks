package BreakBricks.Core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import BreakBricks.Settings.Settings;

/**
 * 
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 * 
 */
public class TransparentCell extends Cell {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Time To Live
	 */
	private static int ttl = 3;

	/**
	 * Timer
	 */
	private Timer timer;

	/**
	 * Create a Transparent Cell on a specific position and random color
	 * 
	 * @param grid
	 *            grid
	 * @param row
	 *            Row of Cell
	 * @param col
	 *            Column of Cell
	 */
	public TransparentCell(Grid grid, int row, int col) {
		super(grid, row, col);
		ttl = Settings.getSCShowTime();
		TimeToLive time = new TimeToLive();

		timer = new Timer(1000, time);
		timer.start();
	}

	/**
	 * Create a Transparent Cell on a specific position and color
	 * 
	 * @param row
	 *            Row of Cell
	 * @param col
	 *            Column of Cell
	 * @param colorIndex
	 *            color index for new Cell, if this colorIndex is not valid a
	 *            random color is used
	 */
	public TransparentCell(Grid grid, int row, int col, int colorIndex) {
		super(grid, row, col, colorIndex);

		TimeToLive time = new TimeToLive();

		timer = new Timer(1000, time);
		timer.start();
	}

	/**
	 * Turn on Timer..
	 */
	void turnOntimer() {
		TimeToLive time = new TimeToLive();

		timer = new Timer(1000, time);
		timer.start();
	}

	@Override
	public int getColorIndex() {
		int row = this.getRow();
		int col = this.getCol();

		if (grid.isEmpty(row, col + 1) == false
				&& grid.getCell(row, col + 1).isMarked() == true)
			return grid.getCell(row, col + 1).getColorIndex();
		if (grid.isEmpty(row, col - 1) == false
				&& grid.getCell(row, col - 1).isMarked() == true)
			return grid.getCell(row, col - 1).getColorIndex();
		if (grid.isEmpty(row + 1, col) == false
				&& grid.getCell(row + 1, col).isMarked() == true)
			return grid.getCell(row + 1, col).getColorIndex();
		if (grid.isEmpty(row - 1, col) == false
				&& grid.getCell(row - 1, col).isMarked() == true)
			return grid.getCell(row - 1, col).getColorIndex();
		return -1;
	}

	/**
	 * Find (recursively) all Cells with the same color-index which are next to
	 * each other
	 */
	@Override
	public void findsame() {
		int row = this.getRow();
		int col = this.getCol();
		timer.stop();

		if (grid.isEmpty(row, col) == true) {
			return;
		}
		grid.marked++;
		grid.setMark(row, col, true);
		int color = grid.getColor(row, col);

		if (grid.getCell(row, col + 1) != null
				&& grid.getCell(row, col + 1).getColorIndex() == color
				&& grid.isMarked(row, col + 1) == false) {
			grid.getCell(row, col + 1).findsame();
		}
		if (grid.getCell(row, col - 1) != null
				&& grid.getCell(row, col - 1).getColorIndex() == color
				&& grid.isMarked(row, col - 1) == false) {
			grid.getCell(row, col - 1).findsame();
		}
		if (grid.getCell(row + 1, col) != null
				&& grid.getCell(row + 1, col).getColorIndex() == color
				&& grid.isMarked(row + 1, col) == false) {
			grid.getCell(row + 1, col).findsame();
		}
		if (grid.getCell(row - 1, col) != null
				&& grid.getCell(row - 1, col).getColorIndex() == color
				&& grid.isMarked(row - 1, col) == false) {
			grid.getCell(row - 1, col).findsame();
		}
	}

	/**
	 * this class Take Care Of the time remains to live
	 * 
	 * @author Mhmad
	 * 
	 */
	public class TimeToLive implements ActionListener {
		public int counter = 0;

		public TimeToLive() {
			counter = ttl;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (counter < 0) {
				timer.stop();
				grid.getGame().setNeedUpdate(true);
				grid.newCell(TransparentCell.this.getRow(),
						TransparentCell.this.getCol(),
						TransparentCell.super.getColorIndex());
			}
			counter--;
		}
	}

	/**
	 * @return the timer
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * @param timer
	 *            the timer to set
	 */
	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
