/**
 * 
 */
package BreakBricks.Statistics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * @author Mhmad al-Najjar,MAMProgr(Mohammed Anas Al-Mahdi),Ammar lakis.
 * 
 */
public class Statistics {

	/**
	 * path of settings files
	 */
	private static String Path = "src/res/Statistics.xml";

	/**
	 * Array of Member
	 */
	private ArrayList<Member> members;

	/**
	 * Default constructor
	 */
	public Statistics() {
		this.members = new ArrayList<>();
	}

	/**
	 * Save To XML File
	 */
	public boolean saveStatistics() {
		Properties props = new Properties();

		for (int i = 0; i < 10; i++) {
			if (i < members.size())
				props.setProperty(Integer.toString(i), members.get(i)
						.toString());
			else
				props.setProperty(Integer.toString(i), " ");
		}

		OutputStream os;
		try {

			// where to store?
			os = new FileOutputStream(Path);

			// store the properties detail into a pre-defined XML file
			props.storeToXML(os, "Settings File", "UTF-8");

			return true;

		} catch (FileNotFoundException e1) {

			return false;

		} catch (IOException e) {

			return false;

		}
	}

	/**
	 * Load from XML
	 */
	public boolean loadStatistics() {
		Properties props = new Properties();

		InputStream is;
		try {
			is = new FileInputStream(Path);

			// load the xml file into properties format
			props.loadFromXML(is);

			members.clear();
			for (int i = 0; i < 10; i++) {
				String temp = "";
				temp = props.getProperty(Integer.toString(i));

				if (temp != "") {
					String[] info = temp.split(",");
					if (info.length == 2)
						this.members.add(new Member(info[0], info[1]));
				}
			}

			Collections.sort(members);
			return true;

		} catch (FileNotFoundException e) {

			return false;

		} catch (InvalidPropertiesFormatException e) {

			return false;

		} catch (IOException e) {

			return false;

		}
	}

	public boolean setNewScore(String name, int score) {
		return false;
	}

	public ArrayList<String[]> getTopTenPlayer() {
		ArrayList<String[]> data = new ArrayList<>();

		for (int i = 0; i < members.size(); i++) {
			String[] temp = { members.get(i).getName(),
					Integer.toString(members.get(i).getScore()) };
			data.add(temp);
		}
		return data;
	}

	public boolean tryInsertPlayer(int score) {
		int i = 0;
		if (members.size() < 10)
			return true;

		while (i < members.size()) {
			if (score > members.get(i).getScore())
				return true;
			i++;
		}

		return false;
	}

	public boolean insertPlayer(String name, int score) {
		members.add(new Member(name, score));
		Collections.sort(members);

		if (members.size() > 10) {
			members.remove(10);
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < members.size(); i++)
			sb.append(members.get(i).toString() + "\\t");

		return sb.toString();
	}

	/**
	 * Holds Name and score
	 * 
	 * @author Mhmad
	 * 
	 */
	private class Member implements Comparable<Member> {
		private String name;
		private int score;

		/**
		 * Default Constructor
		 * 
		 * @param name
		 *            name of player
		 * @param score
		 *            the score
		 */
		public Member(String name, String score) {
			setName(name);
			setScore(score);
		}

		/**
		 * Second Constructor
		 * 
		 * @param name
		 *            name of player
		 * @param score
		 *            the score
		 */
		public Member(String name, int score) {
			setName(name);
			this.score = score;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name
		 *            the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the score
		 */
		public int getScore() {
			return score;
		}

		/**
		 * @param score
		 *            the score to set
		 */
		public void setScore(String score) {
			try {
				this.score = Integer.parseInt(score);
			} catch (NumberFormatException e) {
				this.score = 0;
			}
		}

		@Override
		public int compareTo(Member other) {
			if (this.getScore() < other.getScore())
				return +1;
			else if (this.getScore() > other.getScore())
				return -1;
			return 0;
		}

		@Override
		public String toString() {
			return getName() + "," + Integer.toString(getScore());
		}
	}
}
