package BreakBricks.Settings;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 * 
 */
public class Sound extends Thread {

	/**
	 * cell clips
	 */
	private static Clip[] clips = new Clip[4];

	/**
	 * clips id
	 */
	private int clipID;

	/**
	 * clips name
	 */
	private String clipName;

	/**
	 * is clip must loop continuously..
	 */
	private boolean isContinuously = false;

	/**
	 * default constructor
	 */
	public Sound(String clipName) {
		super();
		this.clipName = clipName;
		switch (clipName) {
		case "Explosive":
			clipID = 0;
			break;
		case "Wrong":
			clipID = 1;
			break;
		case "GameOver":
			clipID = 2;
			break;
		case "BackSound":
			clipID = 3;
			isContinuously = true;
			break;
		}
		start();
	}

	@Override
	public void run() {
		try {
			if (clips[clipID] == null) {
				AudioInputStream stream = AudioSystem
						.getAudioInputStream(getClass().getResource(
								"/res/sounds/" + clipName + ".wav"));

				clips[clipID] = AudioSystem.getClip();
				clips[clipID].open(stream);
				if (isContinuously) {
					clips[clipID].loop(Clip.LOOP_CONTINUOUSLY); // loop
																// continuously..
					Thread.sleep(10000); // looping as long as this thread is
											// alive
				} else {
					clips[clipID].loop(0);
				}
			}
			clips[clipID].setMicrosecondPosition(0);
			clips[clipID].start();
			clips[clipID].drain();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void turnOff() {
		clips[clipID].stop();
	}

	public void turnOn() {
		clips[clipID].start();
	}
}
