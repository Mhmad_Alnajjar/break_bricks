package BreakBricks.Settings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * 
 * @author MAMProgr(Mohammed Anas Al-Mahdi), Mhmad al-Najjar,Ammar lakis.
 * 
 */
public class Settings {

	/**
	 * path of settings files
	 */
	private static String Path = "src/res/settings/";

	/**
	 * grid cols count
	 */
	private static int gridCols;

	/**
	 * grid rows count
	 */
	private static int gridRows;

	/**
	 * colors type
	 */
	private static String colorTypes;

	/**
	 * special cells show time
	 */
	private static int SCShowTime;

	/**
	 * special cells frequency
	 */
	private static int SCFrequency;

	public Settings() {

	}

	public Settings(int gridRows, int gridCols, String colorTypes,
			int SCShowTime, int SCFrequency) {
		Settings.gridRows = gridRows;
		Settings.gridCols = gridCols;
		Settings.colorTypes = colorTypes;
		Settings.SCShowTime = SCShowTime;
		Settings.SCFrequency = SCFrequency;
	}

	/**
	 * @return the gridRows
	 */
	public static int getGridRows() {
		return gridRows;
	}

	/**
	 * @param gridRows
	 *            the gridRows to set
	 */
	public static void setGridRows(int gridRows) {
		Settings.gridRows = gridRows;
	}

	/**
	 * @return the gridCols
	 */
	public static int getGridCols() {
		return gridCols;
	}

	/**
	 * @param gridCols
	 *            the gridCols to set
	 */
	public static void setGridCols(int gridCols) {
		Settings.gridCols = gridCols;
	}

	/**
	 * @return the colorTypes
	 */
	public static String getColorTypes() {
		return colorTypes;
	}

	/**
	 * @param colorTypes
	 *            the colorTypes to set
	 */
	public static void setColorTypes(String colorTypes) {
		Settings.colorTypes = colorTypes;
	}

	/**
	 * @return the sCShowTime
	 */
	public static int getSCShowTime() {
		return SCShowTime;
	}

	/**
	 * @param sCShowTime
	 *            the sCShowTime to set
	 */
	public static void setSCShowTime(int sCShowTime) {
		SCShowTime = sCShowTime;
	}

	/**
	 * @return the sCFrequency
	 */
	public static int getSCFrequency() {
		return SCFrequency;
	}

	/**
	 * @param sCFrequency
	 *            the sCFrequency to set
	 */
	public static void setSCFrequency(int sCFrequency) {
		SCFrequency = sCFrequency;
	}

	/**
	 * save settings into xml file
	 * 
	 * @param fileName
	 *            xml file name
	 * @return True if the saving process has accomplished successfully
	 */
	public static boolean save2XML(String fileName) {

		Properties props = new Properties();

		props.setProperty("gridRows", Integer.toString(Settings.getGridRows()));
		props.setProperty("gridCols", Integer.toString(Settings.getGridCols()));
		props.setProperty("colorTypes", Settings.getColorTypes());
		props.setProperty("SCShowTime",
				Integer.toString(Settings.getSCShowTime()));
		props.setProperty("SCFrequency",
				Integer.toString(Settings.getSCFrequency()));

		OutputStream os;
		try {

			// where to store?
			os = new FileOutputStream(Path + fileName);

			// store the properties detail into a pre-defined XML file
			props.storeToXML(os, "Settings File", "UTF-8");

			return true;

		} catch (FileNotFoundException e1) {

			return false;

		} catch (IOException e) {

			return false;

		}

	}

	/**
	 * load settings from xml file
	 * 
	 * @param fileName
	 *            xml file name
	 * @return loaded settings
	 */
	public static boolean loadXML(String fileName) {

		Properties props = new Properties();

		InputStream is;
		try {
			is = new FileInputStream(Path + fileName);

			// load the xml file into properties format
			props.loadFromXML(is);

			Settings.setGridRows(Integer.parseInt(props.getProperty("gridRows")));
			Settings.setGridCols(Integer.parseInt(props.getProperty("gridCols")));
			Settings.setColorTypes(props.getProperty("colorTypes"));
			Settings.setSCShowTime(Integer.parseInt(props
					.getProperty("SCShowTime")));
			Settings.setSCFrequency(Integer.parseInt(props
					.getProperty("SCFrequency")));

			return true;

		} catch (FileNotFoundException e) {

			return false;

		} catch (InvalidPropertiesFormatException e) {

			return false;

		} catch (IOException e) {

			return false;

		}
	}

	/**
	 * load easy settings..
	 * 
	 * @return true if done.
	 */
	public static boolean loadEasy() {
		return loadXML("easy.xml");
	}

	/**
	 * load normal settings..
	 * 
	 * @return true if done.
	 */
	public static boolean loadNormal() {
		return loadXML("normal.xml");
	}

	/**
	 * load hard settings..
	 * 
	 * @return true if done.
	 */
	public static boolean loadHard() {
		return loadXML("hard.xml");
	}

	/**
	 * @return the path
	 */
	public static String getPath() {
		return Path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public static void setPath(String path) {
		Path = path;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("Settings Details - ");
		sb.append("Grid Rows:" + getGridRows());
		sb.append(", ");
		sb.append("Grid Cols:" + getGridCols());
		sb.append(", ");
		sb.append("Color Type:" + getColorTypes());
		sb.append(", ");
		sb.append("Special Cells Show Time:" + getSCShowTime());
		sb.append(", ");
		sb.append("Special Cells Frequency:" + getSCFrequency());
		sb.append(".");

		return sb.toString();
	}
}
